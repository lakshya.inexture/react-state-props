import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./index.css";

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: "",
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        this.setState({
            input: e.target.value,
        });
    }
    render() {
        return (
            <div>
                <form action="">
                    <input
                        type="text"
                        value={this.state.input}
                        onChange={this.handleChange}
                    />
                    <p>{this.state.input}</p>
                    <button type="submit">Submit</button>
                </form>
            </div>
        );
    }
}

ReactDOM.render(<Input />, document.getElementById("root"));
// glpat-y7P6rA4_4TPveQSPJRNg Gitlab access token valid till 11/2/22
